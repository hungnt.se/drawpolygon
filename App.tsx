import React, {useState, useCallback} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Svg, Polygon} from 'react-native-svg';
import Draggable from './src/Draggable';

export default function App() {
  const [coordinates, setCoordinates] = useState<string[]>([]);

  const triggerSwipe = (event: any) => {
    setCoordinates(
      coordinates.concat(
        `${Math.round(event.nativeEvent.locationX)},${Math.round(
          event.nativeEvent.locationY,
        )}`,
      ),
    );
  };

  const isLineIntersect = (start1, end1, start2, end2) => {
    const delta1x = end1.split(',')[0] - start1.split(',')[0];
    const delta1y = end1.split(',')[1] - start1.split(',')[1];
    const delta2x = end2.split(',')[0] - start2.split(',')[0];
    const delta2y = end2.split(',')[1] - start2.split(',')[1];
    // create a 2D matrix from our vectors and calculate the determinant
    const determinant = delta1x * delta2y - delta2x * delta1y;
    if (Math.abs(determinant) < 0.0001) {
      return false;
    }
    // if the coefficients both lie between 0 and 1 then we have an intersection
    const ab =
      ((start1.split(',')[1] - start2.split(',')[1]) * delta2x -
        (start1.split(',')[0] - start2.split(',')[0]) * delta2y) /
      determinant;

    if (ab > 0 && ab < 1) {
      const cd =
        ((start1.split(',')[1] - start2.split(',')[1]) * delta1x -
          (start1.split(',')[0] - start2.split(',')[0]) * delta1y) /
        determinant;

      if (cd > 0 && cd < 1) {
        // lines cross – figure out exactly where and return it
        const intersectX = start1.x + ab * delta1x;
        const intersectY = start1.y + ab * delta1y;
        return !(intersectX && intersectY);
      }
    }

    return false;
  };

  const checkCoordinatesNotAPolygon = useCallback(() => {
    for (let i = 0; i < coordinates.length; i++) {
      for (let j = 0; j < coordinates.length; j++) {
        if (
          typeof coordinates[i + 1] === 'undefined' ||
          typeof coordinates[j + 1] === 'undefined'
        ) {
          continue;
        }

        const isTrue = isLineIntersect(
          coordinates[i],
          coordinates[i + 1],
          coordinates[j],
          coordinates[j + 1],
        );
        if (isTrue) {
          console.log('true cmnrrrr bro oi');
        }
      }
    }
  }, [coordinates]);

  const onDragElement = useCallback(
    (_event, gestureState, index: number) => {
      let markers = [...coordinates];
      markers[index] = `${Math.round(gestureState.moveX)},${Math.round(
        gestureState.moveY,
      )}`;
      setCoordinates(markers);
      checkCoordinatesNotAPolygon();
      // console.log(coordinates);
    },
    [checkCoordinatesNotAPolygon, coordinates],
  );

  // console.log(
  //   'hell world =====>',
  //   isLineIntersect('196,184', '333,291', '217,419', '39,300'),
  // );
  // console.log(
  //   'hell world =====>',
  //   isLineIntersect('18,186', '326,195', '192,82', '235,379'),
  // );
  // console.log(
  //   'hello world ',
  //   isLineIntersect('301,377', '111,320', '111,320', '63,175'),
  // );
  return (
    <TouchableOpacity
      style={{
        flex: 1,
        width: '100%',
        height: '100%',
        zIndex: 1,
      }}
      onPress={(event) => triggerSwipe(event)}>
      <View>
        <Svg>
          <Polygon
            points={coordinates.join(' ')}
            fill="lime"
            stroke="purple"
            strokeWidth="1"
          />
        </Svg>
        {coordinates.map((item, index) => {
          return (
            <Draggable
              key={index}
              x={Number(item.split(',')[0]) - 7}
              y={Number(item.split(',')[1]) - 7}
              isCircle
              onDrag={(event, gestureState) =>
                onDragElement(event, gestureState, index)
              }
              renderSize={14}
              renderColor="gray"
            />
          );
        })}
      </View>
    </TouchableOpacity>
  );
}
